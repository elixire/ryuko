#!/usr/bin/env python3

import sys
import json
import string
from typing import List
from pathlib import Path
from PIL import Image, ImageFont, ImageDraw


ALPHABET = string.ascii_letters + string.punctuation + string.digits + " "


def main(argv: List[str]):
    try:
        self_path = Path(argv[0])
        font_path = Path(argv[1])
        bold_font_path = Path(argv[2])
        font_point_size = int(argv[3])

        output_fontdata_path = Path(argv[4])
        output_spritesheet_path = Path(argv[5])
    except (ValueError, IndexError):
        print(
            f"usage: {argv[0]} font_path bold_font_path font_point_size output_fontdata_path output_spritesheet_path"
        )
        sys.exit(1)

    font = ImageFont.truetype(str(font_path), size=font_point_size)
    bold_font = ImageFont.truetype(str(bold_font_path), size=font_point_size)

    single_letter_font_dimensions = font.getsize("0")
    font_width, _ = font.getsize("0")
    metrics = font.getmetrics()
    font_height = metrics[0] + metrics[1]

    letter_coords = []
    fontdata = {
        "metadata_version": 2,
        "font_dimensions": [font_width, font_height],
        "letter_coords": letter_coords,
    }

    x, y = 0, 0

    # (* 2) is done because we build a whole spritesheet for each variation
    # of the font:
    #  - nothing
    #  - bold
    image = Image.new("RGBA", (font_width * len(ALPHABET) * 2, font_height))
    draw = ImageDraw.Draw(image)

    for letter in ALPHABET:
        letter_coords.append(
            {"letter": letter, "x": x, "y": y, "bold": False, "italic": False}
        )
        draw.rectangle((x, y, x + font_width, y + font_height), fill=(0, 0, 0, 255))
        draw.text((x, y), letter, fill=(255, 255, 255, 255), font=font)
        x += font_width

    for letter in ALPHABET:
        letter_coords.append(
            {"letter": letter, "x": x, "y": y, "bold": True, "italic": False}
        )
        draw.rectangle((x, y, x + font_width, y + font_height), fill=(0, 0, 0, 255))
        draw.text((x, y), letter, fill=(255, 255, 255, 255), font=bold_font)
        x += font_width

    image.save(str(output_spritesheet_path))

    with open(output_fontdata_path, "w") as fontdata_file:
        json.dump(fontdata, fontdata_file)


if __name__ == "__main__":
    main(sys.argv)
