# contributing guidelines

hello! we're glad you wish to contribute to this little thing of ours.

## creating a new effect

### the effect interface

ryuko's effect architecture enables effects to be written in any language,
as long as it follows a general interface:

- the 0th argument is the filename of the effect
- the 1st argument is the path to the file being processed
- 2nd to Nth arguments are possible parameters the effect may have

effects MUST write back to the same file as the result (on imagemagick,
using `convert ... "$1" "$1"` or `mogrify`)

as exit codes (TODO add some interface for effect specific errors
e.g argument validation, can't be done via exit codes):

- 0 is a successful run
- 1 is a general failure

### effects in the codebase

inside the `effects/src` directory, you may create a folder with the name of
your effect, containing your effects' source code inside. the folder can export
multiple effects

#### effects with parameters

if your effect has parameters, the arity (the amount of parameters) SHOULD be
registered in `src/effect.cr`'s `PARAMETRIZED_EFFECTS` hash.

#### effects written in ahead-of-time compiled languages (C, Rust, C++, Zig, ...)

(TODO: we may move to build.sh instead of Makefiles)

- the effect project folder MUST have a `.gitignore` with `bin/`
- the exported binaries MUST live in `bin/`

#### effects written in scripting languages (shell, python)

if your effect can be run via shebangs (for example, `#!/usr/bin/env python3`),
it can live standalone in the effect folder, e.g `effects/src/jpgify/jpgify`.

this also applies to effects written in shell.

##### shell guidelines

- shell scripts MUST be POSIX compliant.
- use `#!/bin/sh`
- make sure to have `set -o nounset`
- make sure to have `set -o errexit`
- if using tools like awk, also assume they are POSIX awk.
