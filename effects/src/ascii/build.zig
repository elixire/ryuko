const Builder = @import("std").build.Builder;

pub fn build(b: *Builder) void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = b.standardTargetOptions(.{});

    // Standard release options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall.
    const mode = b.standardReleaseOptions();

    const exe = b.addExecutable("ascii", "src/main.zig");
    const test_exe = b.addTest("src/main.zig");

    exe.linkSystemLibrary("GraphicsMagickWand");
    exe.linkSystemLibrary("GraphicsMagick");
    exe.linkSystemLibrary("vips");
    exe.linkLibC();
    exe.setTarget(target);
    exe.setBuildMode(mode);
    exe.install();

    test_exe.linkSystemLibrary("GraphicsMagickWand");
    test_exe.linkSystemLibrary("GraphicsMagick");
    test_exe.linkLibC();
    test_exe.setTarget(target);
    test_exe.setBuildMode(mode);

    const run_cmd = exe.run();
    run_cmd.step.dependOn(b.getInstallStep());
    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);

    const test_step = b.step("test", "test");
    test_step.dependOn(&test_exe.step);
}
