const std = @import("std");
const main = @import("./main.zig");

pub const c = @cImport({
    @cInclude("vips/vips.h");
});

pub const VipsBackend = struct {
    const Context = struct {
        image: *c.VipsImage,

        font_dimensions: main.Dimensions = undefined,

        foreground: [3]u8 = undefined,
        background: [3]u8 = undefined,
    };

    pub fn initAPI() void {
        var args_it = std.process.args();
        if (c.vips_init(args_it.nextPosix().?) == -1) @panic("vips init fail");
    }

    pub fn deinitAPI() void {
        c.vips_shutdown();
    }

    pub fn createContext() !Context {
        var image = c.vips_image_new_memory();
        return Context{ .image = image };
    }

    pub fn deinitContext(ctx: *Context) void {
        c.g_object_unref(ctx.image);
    }

    pub fn initImage(
        ctx: *Context,
        width: usize,
        height: usize,
        font_pt_size: usize,
        dimensions: main.Dimensions,
    ) !void {
        ctx.font_dimensions = dimensions;
    }

    pub fn setCurrentColors(ctx: *Context, foreground: [3]u8, background: [3]u8) !void {}

    pub fn drawText(ctx: *Context, x: usize, y: usize, text: []const u8, text_cstr: [:0]const u8) !void {
        // draw underlying recangle
        var ink =
            [3]f64{ 255, 255, 255 };
        if (c.vips_draw_rect(
            ctx.image,
            &ink,
            3,
            @intCast(c_int, x),
            @intCast(c_int, y),
            @intCast(c_int, x + ctx.font_dimensions.width),
            @intCast(c_int, y + ctx.font_dimensions.height),
        ) == -1) {
            //const errmsg = c.vips_error_buffer();
            //std.log.err("vips error: {s}", .{errmsg});
            return error.DrawRectFail;
        }

        //if (c.vips_text(ctx.image, text_cstr) == -1) @panic("shit");
    }

    pub fn finishDrawing(ctx: *Context) !void {}

    pub fn saveImage(ctx: *Context, path_cstr: [:0]const u8) !void {}
};
