const std = @import("std");

const magick = @import("./magick.zig");
const vips = @import("./vips.zig");
const ansi = @import("./ansi.zig");

const FONT_POINT_SIZE = 24;

const Backend = magick.MagickBackend;

pub const ResourceInfo = struct {
    metadata_path: []const u8,
    spritesheet_path: []const u8,
};

fn fetchResourcePaths(self_path: []const u8, allocator: *std.mem.Allocator) !ResourceInfo {
    const self_folder = std.fs.path.dirname(self_path);

    const font_path = try std.fmt.allocPrint(
        allocator,
        "{s}/../resources/ascii/monogram.ttf",
        .{self_folder},
    );

    const metadata_path = try std.fmt.allocPrint(
        allocator,
        "{s}/../resources/ascii/font_metadata.json",
        .{self_folder},
    );

    const spritesheet_path = try std.fmt.allocPrint(
        allocator,
        "{s}/../resources/ascii/font_spritesheet.png",
        .{self_folder},
    );

    return ResourceInfo{
        .metadata_path = metadata_path,
        .spritesheet_path = spritesheet_path,
    };
}

pub const Dimensions = struct {
    width: usize,
    height: usize,
};

fn fetchFontDimensions(font_path_cstr: [:0]u8) !Dimensions {
    var pixel_wand = magick.c.NewPixelWand();
    defer magick.c.DestroyPixelWand(pixel_wand);

    var drawing_wand = magick.c.NewDrawingWand();
    defer magick.c.DestroyDrawingWand(drawing_wand);

    var magick_wand = magick.c.NewMagickWand();
    defer {
        _ = magick.c.DestroyMagickWand(magick_wand);
    }

    _ = magick.c.MagickSetSize(magick_wand, 300, 100);
    _ = magick.c.MagickReadImage(magick_wand, "xc:white");

    _ = magick.c.PixelSetColor(pixel_wand, "white");
    _ = magick.c.MagickDrawSetFillColor(drawing_wand, pixel_wand);
    _ = magick.c.MagickDrawSetFont(drawing_wand, font_path_cstr.ptr);
    _ = magick.c.MagickDrawSetFontSize(drawing_wand, FONT_POINT_SIZE);

    // magick api returns a doubleptr with a fixed size of 7
    // good api. pls, help.
    var font_metrics_data = magick.c.MagickQueryFontMetrics(magick_wand, drawing_wand, " ");
    defer {
        _ = magick.c.MagickRelinquishMemory(font_metrics_data);
    }
    std.debug.assert(font_metrics_data != null);

    // from monogram, i want a wxh of 9x16px to feed to img2txt

    std.log.info("{}", .{font_metrics_data});
    std.log.info("{} {} {} {} {} {} {} {}", .{
        font_metrics_data[0],
        font_metrics_data[1],
        font_metrics_data[2],
        font_metrics_data[3],
        font_metrics_data[4],
        font_metrics_data[5],
        font_metrics_data[6],
        font_metrics_data[7],
    });
    const font_width = @floatToInt(u32, font_metrics_data[4]);
    const font_height = @floatToInt(u32, font_metrics_data[5] + font_metrics_data[3]);
    std.log.info("wxh of font {}x{}", .{ font_width, font_height });

    return Dimensions{
        .width = font_width,
        .height = font_height,
    };
}

fn runProgram(
    image_path: []const u8,
    term_width: usize,
    dimensions: Dimensions,
    allocator: *std.mem.Allocator,
) ![]const u8 {
    var string_buffer: [128]u8 = undefined;
    var fba = std.heap.FixedBufferAllocator.init(&string_buffer);
    const buf_alloc = &fba.allocator;
    const term_width_str = try std.fmt.allocPrint(buf_alloc, "{d}", .{term_width});
    const font_width_str = try std.fmt.allocPrint(buf_alloc, "{d}", .{dimensions.width});
    const font_height_str = try std.fmt.allocPrint(buf_alloc, "{d}", .{dimensions.height});

    const args = [_][]const u8{
        "img2txt",
        image_path,
        "-f",
        "utf8",
        "-d",
        "fstein",
        "-W",
        term_width_str,
        "-x",
        font_width_str,
        "-y",
        font_height_str,
    };

    std.log.info("args = {s}", .{args});

    var proc = try std.ChildProcess.init(&args, allocator);
    defer proc.deinit();

    proc.stdout_behavior = .Pipe;
    proc.stderr_behavior = .Inherit;

    std.log.info("spawn", .{});

    try proc.spawn();
    std.log.info("wait", .{});

    var stdout_data = try proc.stdout.?.reader().readAllAlloc(allocator, std.math.maxInt(usize));
    std.log.info("got data {}: \n{s}", .{ stdout_data.len, stdout_data });

    const term_result = try proc.wait();

    switch (term_result) {
        .Exited => |term_code| {
            std.log.info("img2txt exited with error code: {d}", .{term_code});
            if (term_code != 0) @panic("fuck");
        },
        else => unreachable,
    }

    return stdout_data;
}
const CursorState = struct {
    x: usize = 0,
    y: usize = 0,
};

const TerminalDimensions = struct {
    lines: usize,
    columns: usize,
};

fn calculateTerminalDimensions(emitter: *ansi.Emitter) !TerminalDimensions {
    var columns: usize = 0;
    var lines: usize = 0;

    // we haven't read the entire stdout yet, so we don't know the line x width
    // dimensions of the screen. iterate through the whole thing, find the
    // line breaks and first line length, so that we can determine complete
    // pixel width x height of the output image.

    var timer = try std.time.Timer.start();
    while (true) {
        var instruction = emitter.nextInstruction();
        switch (instruction) {
            .eof => break,
            .line_break => lines += 1,
            .write_text => |text| {
                if (lines != 0) continue;
                columns = text.len;
            },
            else => {},
        }
    }

    // reset the scanner other users can read from it
    emitter.scanner.reset();

    const screen_loop_ns = timer.read();
    std.log.info("screen dimension loop took {d}ms", .{
        screen_loop_ns / std.time.ns_per_ms,
    });
    timer.reset();

    std.log.info("lines x cols: {} x {}", .{ lines, columns });

    std.debug.assert(lines > 0);
    std.debug.assert(columns > 0);
    return TerminalDimensions{ .lines = lines, .columns = columns };
}

pub fn writeOutputNew(
    emitter: *ansi.Emitter,
    allocator: *std.mem.Allocator,
    metadata: SpritesheetMetadata,
    spritesheet_path: []const u8,
    font_dimensions: Dimensions,
    output_path_cstr: [:0]const u8,
) !void {
    var timer = try std.time.Timer.start();
    const termDimensions = try calculateTerminalDimensions(emitter);

    // we know total amount of lines and columns, with font dimension we can
    // determine the entire size of the image and allocate it under magick

    const image_width = termDimensions.columns * font_dimensions.width;
    const image_height = termDimensions.lines * font_dimensions.height;
    std.log.info(
        "output width x height: {} x {}",
        .{ image_width, image_height },
    );

    var ctx = try Backend.createContext(allocator);
    defer Backend.deinitContext(&ctx);

    var cstr_buffer: [1024]u8 = undefined;
    var fba = std.heap.FixedBufferAllocator.init(&cstr_buffer);
    const alloc = &fba.allocator;

    try Backend.initSpritesheet(
        &ctx,
        metadata,
        try std.cstr.addNullByte(alloc, spritesheet_path),
    );
    try Backend.initCanvas(&ctx, image_width, image_height);

    // TODO
    // MagickCompositeImage on drawText
    // MagickTransparentImage on drawText (also clone the image beforehand)
    // MagickCropImage on createSprites

    var state = CursorState{ .y = font_dimensions.height };
    while (true) {
        var instruction = emitter.nextInstruction();
        //std.log.info("inst: {}", .{instruction});

        switch (instruction) {
            .eof => break,
            .set_color => |color_data| try Backend.setCurrentColors(&ctx, color_data.foreground, color_data.background),
            .line_break => {
                state.x = 0;
                state.y += font_dimensions.height;
            },
            .write_text => |text| {
                var text_cstr = try std.cstr.addNullByte(alloc, text);
                defer fba.reset();

                try Backend.drawText(&ctx, state.x, state.y, text, text_cstr);
                state.x += font_dimensions.width * text_cstr.len;
            },
        }
    }

    const draw_loop_ns = timer.read();
    std.log.info("main draw loop took {d}ms", .{
        draw_loop_ns / std.time.ns_per_ms,
    });
    timer.reset();

    try Backend.finishDrawing(&ctx);

    const magick_draw_ns = timer.read();
    std.log.info("gm draw took {d}ms", .{
        magick_draw_ns / std.time.ns_per_ms,
    });
    timer.reset();

    try Backend.saveImage(&ctx, output_path_cstr);

    const magick_write_ns = timer.read();
    std.log.info("gm write took {d}ms", .{
        magick_write_ns / std.time.ns_per_ms,
    });
    timer.reset();
}

pub fn writeOutput(
    emitter: *ansi.Emitter,
    dimensions: Dimensions,
    output_path_cstr: [:0]const u8,
) !void {
    var timer = try std.time.Timer.start();
    const termDimensions = try calculateTerminalDimensions(emitter);

    // we know total amount of lines and columns, with font dimension we can
    // determine the entire size of the image and allocate it under magick

    const image_width = termDimensions.columns * dimensions.width;
    const image_height = termDimensions.lines * dimensions.height;
    std.log.info(
        "output width x height: {} x {}",
        .{ image_width, image_height },
    );

    var ctx = try Backend.createContext();
    defer Backend.deinitContext(&ctx);

    try Backend.initImage(&ctx, image_width, image_height, FONT_POINT_SIZE, dimensions);

    var cstr_buffer: [1024]u8 = undefined;
    var fba = std.heap.FixedBufferAllocator.init(&cstr_buffer);
    const alloc = &fba.allocator;

    var state = CursorState{ .y = dimensions.height };
    while (true) {
        var instruction = emitter.nextInstruction();
        // std.log.info("inst: {}", .{instruction});

        switch (instruction) {
            .eof => break,
            .set_color => |color_data| {
                try Backend.setCurrentColors(&ctx, color_data.foreground, color_data.background);
            },
            .line_break => {
                state.x = 0;
                state.y += dimensions.height;
            },
            .write_text => |text| {
                var text_cstr = try std.cstr.addNullByte(alloc, text);
                defer fba.reset();

                try Backend.drawText(&ctx, state.x, state.y, text, text_cstr);
                state.x += dimensions.width * text_cstr.len;
            },
        }
    }

    const draw_loop_ns = timer.read();
    std.log.info("main draw loop took {d}ms", .{
        draw_loop_ns / std.time.ns_per_ms,
    });
    timer.reset();

    try Backend.finishDrawing(&ctx);

    const magick_draw_ns = timer.read();
    std.log.info("gm draw took {d}ms", .{
        magick_draw_ns / std.time.ns_per_ms,
    });
    timer.reset();

    try Backend.saveImage(&ctx, output_path_cstr);

    const magick_write_ns = timer.read();
    std.log.info("gm write took {d}ms", .{
        magick_write_ns / std.time.ns_per_ms,
    });
    timer.reset();
}

pub const LetterPosition = struct {
    letter: []const u8,
    x: usize,
    y: usize,
};

pub const SpritesheetMetadata = struct {
    metadata_version: usize,
    font_dimensions: [2]usize,
    letter_coords: []LetterPosition,
};

pub fn main() anyerror!void {
    Backend.initAPI();
    defer Backend.deinitAPI();

    var args = std.process.args();
    const self_path = args.nextPosix() orelse unreachable;
    const image_path = args.nextPosix() orelse @panic("Expected image path argument");
    const terminal_width_arg_opt = args.nextPosix();

    var terminal_width: usize = undefined;
    if (terminal_width_arg_opt) |terminal_width_arg| {
        const terminal_width_arg_int = try std.fmt.parseInt(usize, terminal_width_arg, 10);
        terminal_width = std.math.min(1000, std.math.max(10, terminal_width_arg_int));
    } else {
        terminal_width = 160;
    }

    // get resource paths
    var resource_buffer: [1024]u8 = undefined;
    var resource_alloc = std.heap.FixedBufferAllocator.init(&resource_buffer);
    const resources = try fetchResourcePaths(self_path, &resource_alloc.allocator);
    std.log.info("resource paths: {s} {s}", .{ resources.metadata_path, resources.spritesheet_path });

    var allocator_instance = std.heap.GeneralPurposeAllocator(.{}){};
    defer {
        _ = allocator_instance.deinit();
    }
    const allocator = &allocator_instance.allocator;

    // img2txt needs the font dimension info that is in the spritesheet metadata
    var metadata_file = try std.fs.cwd().openFile(
        resources.metadata_path,
        .{ .read = true, .write = false },
    );
    defer metadata_file.close();

    const metadata_string = try metadata_file.readToEndAlloc(allocator, std.math.maxInt(usize));
    defer allocator.free(metadata_string);
    std.log.info("{s}", .{metadata_string});

    var token_stream = std.json.TokenStream.init(metadata_string);
    var metadata_buffer: [1024 * 10]u8 = undefined;
    var metadata_alloc = std.heap.FixedBufferAllocator.init(&metadata_buffer);
    const metadata = try std.json.parse(
        SpritesheetMetadata,
        &token_stream,
        .{ .allocator = &metadata_alloc.allocator },
    );

    var dimensions = Dimensions{
        .width = metadata.font_dimensions[0],
        .height = metadata.font_dimensions[1],
    };

    // call img2txt!
    var timer = try std.time.Timer.start();
    var data = try runProgram(image_path, terminal_width, dimensions, allocator);
    defer allocator.free(data);

    const command_run_time_ns = timer.read();
    std.log.info("img2txt took {d}ms", .{
        command_run_time_ns / std.time.ns_per_ms,
    });

    timer.reset();
    var scanner = ansi.AnsiScanner.init(data);
    var emitter = ansi.Emitter.init(&scanner);

    const image_path_cstr = try std.cstr.addNullByte(allocator, image_path);
    defer allocator.free(image_path_cstr);

    try writeOutputNew(
        &emitter,
        allocator,
        metadata,
        resources.spritesheet_path,
        dimensions,
        image_path_cstr,
    );
    const draw_time_ns = timer.read();
}
