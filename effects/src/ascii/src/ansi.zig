const std = @import("std");

pub const Token = union(enum) {
    text: []const u8,
    line_break: void,
    ansi_sgr: []const u8,
    eof: void,
};

pub const AnsiScanner = struct {
    source: []const u8,

    start: usize = 0,
    current: usize = 0,

    const Self = @This();

    pub fn init(source: []const u8) Self {
        return Self{
            .source = source,
        };
    }

    pub fn reset(self: *Self) void {
        self.start = 0;
        self.current = 0;
    }

    fn advance(self: *Self) u8 {
        self.current += 1;
        return self.source[self.current - 1];
    }

    fn rollback(self: *Self) void {
        self.current -= 1;
    }

    fn currentLexeme(self: *Self) []const u8 {
        return self.source[self.start..self.current];
    }

    fn peekNext(self: *Self) u8 {
        if (self.current + 1 > self.source.len) return 0;
        return self.source[self.current];
    }

    fn isAtEnd(self: *Self) bool {
        return self.current >= self.source.len;
    }

    fn doAnsi(self: *Self) Token {
        const ansi_start = self.advance();
        std.debug.assert(ansi_start == '[');

        const sgr_start_index = self.current;
        while (self.peekNext() != 'm') {
            _ = self.advance();
        }
        const sgr_end_index = self.current;

        // consume ending 'm'
        _ = self.advance();

        const sgr_lexeme = self.source[sgr_start_index..sgr_end_index];

        return Token{ .ansi_sgr = sgr_lexeme };
    }

    fn doCharacters(self: *Self) Token {
        // TODO: better condition
        while (true) {
            if (self.peekNext() == 0x1b or self.peekNext() == '\n') break;
            _ = self.advance();
        }

        return Token{ .text = self.currentLexeme() };
    }

    pub fn nextToken(self: *Self) Token {
        self.start = self.current;

        if (self.isAtEnd()) return Token{ .eof = {} };

        const character = self.advance();
        return switch (character) {
            0x1b => self.doAnsi(),
            '\n' => Token{ .line_break = {} },
            else => self.doCharacters(),
        };
    }
};

test "it works" {
    const DATA = "\x1b[1;1;1;1;1m";
    var scanner = AnsiScanner.init(DATA);
    var token = scanner.nextToken();
    switch (token) {
        .ansi_sgr => |sgr_data| {
            std.testing.expectEqualStrings("1;1;1;1;1", sgr_data);
        },
        else => unreachable,
    }
}

pub const SetColor = struct {
    background: RGB,
    foreground: RGB,
};

pub const Instruction = union(enum) {
    eof: void,
    write_text: []const u8,
    line_break: void,

    // ansi stuff here
    set_color: SetColor,
};

const RGB = [3]u8;

fn fromHex(comptime hex_string: []const u8) RGB {
    const result = [_]u8{
        std.fmt.parseInt(u8, hex_string[1..3], 16) catch unreachable,
        std.fmt.parseInt(u8, hex_string[3..5], 16) catch unreachable,
        std.fmt.parseInt(u8, hex_string[5..7], 16) catch unreachable,
    };

    return result;
}

const PALETTE = [_]RGB{
    comptime fromHex("#000000"),
    comptime fromHex("#800000"),
    comptime fromHex("#008000"),
    comptime fromHex("#808000"),
    comptime fromHex("#000080"),
    comptime fromHex("#800080"),
    comptime fromHex("#008080"),
    comptime fromHex("#c0c0c0"),
    comptime fromHex("#808080"),
    comptime fromHex("#ff0000"),
    comptime fromHex("#00ff00"),
    comptime fromHex("#ffff00"),
    comptime fromHex("#0000ff"),
    comptime fromHex("#ff00ff"),
    comptime fromHex("#00ffff"),
    comptime fromHex("#ffffff"),
};

fn fromPaletteIndex(index: usize) RGB {
    return PALETTE[index];
}

pub const Emitter = struct {
    scanner: *AnsiScanner,

    const Self = @This();
    pub fn init(scanner: *AnsiScanner) Self {
        return Self{ .scanner = scanner };
    }

    fn emitSGR(self: *Self, data: []const u8) Instruction {
        if (std.mem.eql(u8, data, "0") or data.len == 0) {
            return Instruction{
                .set_color = SetColor{
                    .foreground = [_]u8{ 255, 255, 255 },
                    .background = [_]u8{ 0, 0, 0 },
                },
            };
        }

        var foreground: ?RGB = null;
        var background: ?RGB = null;

        var it = std.mem.split(data, ";");
        while (it.next()) |sgr_command_str| {
            const command = std.fmt.parseInt(usize, sgr_command_str, 10) catch @panic("invalid sgr command not a str");

            if (command == 0 and command == 1 or command == 3 or command == 5) {
                continue;
            } else if (30 <= command and command <= 37) {
                foreground = fromPaletteIndex(command - 30);
            } else if (40 <= command and command <= 47) {
                background = fromPaletteIndex(command - 40);
            } else if (90 <= command and command <= 97) {
                foreground = fromPaletteIndex(command - 90 + 8);
            } else if (100 <= command and command <= 107) {
                background = fromPaletteIndex(command - 100 + 8);
            }
        }

        std.debug.assert(foreground != null);
        std.debug.assert(background != null);
        return Instruction{
            .set_color = SetColor{
                .foreground = foreground.?,
                .background = background.?,
            },
        };
    }

    pub fn nextInstruction(self: *Self) Instruction {
        const token = self.scanner.nextToken();
        switch (token) {
            .eof => return Instruction{ .eof = {} },
            .line_break => return Instruction{ .line_break = {} },
            .ansi_sgr => |data| return self.emitSGR(data),
            .text => |text| return Instruction{ .write_text = text },
        }
    }
};
